import './App.css';

import React, { useReducer } from 'react';

import {
  initialState,
  SignInDispatchContext,
  signInReducer,
  SignInStateContext,
} from './AuthProvider';
import SignIn from './SignIn';
import UserInfoPanel from './UserInfoPanel';

function App() {
  const [state, dispatch] = useReducer(signInReducer, initialState);
  const { isAuthenticated, userInfo } = state;
  return (
    <SignInStateContext.Provider value={state}>
      <SignInDispatchContext.Provider value={dispatch}>
        <div className="App">
          {isAuthenticated && userInfo ? <UserInfoPanel /> : <SignIn />}
        </div>
      </SignInDispatchContext.Provider>
    </SignInStateContext.Provider>
  );
}

export default App;
