import React from 'react';

import { SESSION_KEYS } from './utils/constants';

export const initialState = {
  error: null,
  expires: null,
  isAuthenticated: false,
  isSigningIn: false,
  projectInfo: null,
  refreshToken: null,
  accessToken: null,
  userInfo: null,
};

export const actions = {
  PROJECT_INFO_SUCCESS: "PROJECT_INFO_SUCCESS",
  SIGN_IN: "SIGN_IN",
  SIGN_IN_SUCCESS: "SIGN_IN_SUCCESS",
  SIGN_IN_ERROR: "SIGN_IN_ERROR",
  SIGN_OUT: "SIGN_OUT",
  USER_INFO_SUCCESS: "USER_INFO_SUCCESS",
};

export const signInReducer = (state, action) => {
  switch (action.type) {
    case actions.SIGN_IN:
      return {
        ...state,
        isSigningIn: true,
      };
    case actions.SIGN_IN_SUCCESS:
      sessionStorage.setItem(
        SESSION_KEYS.GITLAB_ACCESS_TOKEN,
        JSON.stringify(action.payload)
      );
      return {
        ...state,
        expires: action.payload.expires,
        isAuthenticated: true,
        refreshToken: action.payload.refreshToken,
        token: action.payload.token,
        userInfo: null,
      };
    case actions.USER_INFO_SUCCESS:
      sessionStorage.setItem("userInfo", JSON.stringify(action.payload));
      return {
        ...state,
        userInfo: action.payload,
        isSigningIn: false,
      };
    case actions.PROJECT_INFO_SUCCESS:
      return {
        ...state,
        projectInfo: action.payload,
      };
    case actions.SIGN_IN_ERROR:
      return {
        ...state,
        error: action.payload.error,
      };
    case actions.SIGN_OUT:
      Object.values(SESSION_KEYS).forEach((v) => sessionStorage.removeItem(v));
      return initialState;
    default:
      return state;
  }
};

// @ts-ignore
export const SignInStateContext = React.createContext();
// @ts-ignore
export const SignInDispatchContext = React.createContext();
