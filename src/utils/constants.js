/**
 * Names of browser session keys
 */
export const SESSION_KEYS = {
  GITLAB_CODE_VERIFIER: "gitlab_code_verifier",
  GITLAB_STATE: "gitlab_state",
  GITLAB_ACCESS_TOKEN: "gitlab_access_tokens",
  USER_INFO: "gitlab_userinfo",
};
