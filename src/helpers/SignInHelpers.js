import { encode as base64encode } from 'base64-arraybuffer';
import fetchIntercept from 'fetch-intercept';

import { SESSION_KEYS } from '../utils/constants';

const REDIRECT_URI =
  window.location.origin + process.env.REACT_APP_CALLBACK_URI;
const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;

/**
 * A simple function to generate a random string used
 * in the GitLab SSO Sign In Process.
 * @link {https://datatracker.ietf.org/doc/html/rfc7636#section-1.1}
 *
 * @param {number?} [stringLength=48] length of string to return. Default 48
 * @returns {string} A random string in code verifier format
 */
const makeCodeVerifierString = (stringLength = 48) => {
  let codeVerifier = "";
  let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < stringLength; i += 1) {
    codeVerifier += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return codeVerifier;
};

/**
 * Generate a code challenge that will return
 * the codeVerifier string. Used in the SSO handshake process
 *
 * @param {string} codeVerifier
 * @returns {Promise<string>} codeChallenge string
 */
const generateCodeChallenge = async (codeVerifier) => {
  const encoder = new TextEncoder();
  const data = encoder.encode(codeVerifier);
  const digest = await window.crypto.subtle.digest("SHA-256", data);
  const base64Digest = base64encode(digest);
  return base64Digest.replace(/\+/g, "-").replace(/\//g, "_").replace(/=/g, "");
};

/**
 * Fetch user information from the GitLab user API page
 *
 * @returns {Promise<Object>} json data on user
 */
export async function getGitLabUserInfo() {
  const response = await fetch("https://gitlab.com/api/v4/user/");
  const jsonInfo = await response.json();

  if (response.status === 200) {
    return jsonInfo;
  } else {
    let errorMsg = `Could not get user info ${
      jsonInfo && jsonInfo.error ? jsonInfo.error : ""
    }. Details: ${
      jsonInfo && jsonInfo.error_description ? jsonInfo.error_description : ""
    }.`;
    return { error: errorMsg };
  }
}

export async function getGitLabProjectInfo() {
  const response = await fetch("https://gitlab.com/api/v4/projects/");
  const jsonInfo = await response.json();

  if (response.status === 200) {
    return jsonInfo;
  } else {
    let errorMsg = `Could not get project info ${
      jsonInfo && jsonInfo.error ? jsonInfo.error : ""
    }. Details: ${
      jsonInfo && jsonInfo.error_description ? jsonInfo.error_description : ""
    }.`;
    return { error: errorMsg };
  }
}

/**
 * Navigate to Gitlab's oauth authorize url endpoint
 * with appropriate args.
 * (see: https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-with-proof-key-for-code-exchange-pkce)
 * GitLab will prompt the user to login with their creds and ask for approval to send us certain information.
 *
 * @param {string} [redirectUri=REACT_APP_CALLBACK_URI] Optional, callback uri for gitlab
 */
export async function getGitLabAuthorization(redirectUri = REDIRECT_URI) {
  const codeVerifier = makeCodeVerifierString();
  const codeChallenge = await generateCodeChallenge(codeVerifier);
  const gitlabState = makeCodeVerifierString(12);

  sessionStorage.setItem(SESSION_KEYS.GITLAB_CODE_VERIFIER, codeVerifier);
  sessionStorage.setItem(SESSION_KEYS.GITLAB_STATE, gitlabState);

  const params = new URLSearchParams({
    client_id: CLIENT_ID,
    code_challenge: codeChallenge,
    code_challenge_method: "S256",
    code_verifier: codeVerifier,
    redirect_uri: redirectUri,
    response_type: "code",
    scope: "read_user read_api", // https://docs.gitlab.com/ee/integration/oauth_provider.html#authorized-applications
    state: gitlabState,
  });

  const gitlabUrl = "https://gitlab.com/oauth/authorize?" + params.toString();
  window.location.href = gitlabUrl;
}

/**
 * extract query params from the browser url. Try to parse
 * the gitlab code and state params
 *
 * @param {string} urlParams url params such as ?f=json&code=w329ab_1&state=aBBPioer34kk
 * @returns {{code: string, gitlabState: string}} callback variables from gitlab
 */
export function getParamsFromGitLabCallback(urlParams) {
  const searchParams = new URLSearchParams(urlParams);

  let code;
  let gitlabState;
  searchParams.forEach((value, key) => {
    if (key === "code") {
      code = value;
    } else if (key === "state") {
      gitlabState = value;
    }
  });

  return { code, gitlabState };
}

/**
 * Fetch tokens from gitlab based on the gitlab provided
 * tokenParams (code, code_verifier)
 *
 * @param {string} code GitLab Code
 * @param {string} codeVerifier GitLab CodeVerifier
 * @param {string?} [clientId] Optional: GitLab Client Id: defaults to pulling from .env file (REACT_APP_CLIENT_ID)
 * @param {string?} [redirectUri] Optional: redirect Uri, where callback will go, defaults to REDIRECT_URI
 * @returns {Promise<Object>} json of fetch data
 */
export async function fetchGitLabToken(
  code,
  codeVerifier,
  clientId = CLIENT_ID,
  redirectUri = REDIRECT_URI
) {
  let tokenParams = {
    client_id: clientId,
    code: code,
    code_verifier: codeVerifier,
    grant_type: "authorization_code",
    redirect_uri: redirectUri,
  };
  let response = await fetch("https://gitlab.com/oauth/token", {
    body: JSON.stringify(tokenParams),
    headers: {
      "Content-Type": "application/json",
    },
    method: "POST",
  });
  try {
    const json = await response.json();
    return json;
  } catch (e) {
    console.error("An error occurred fetching the gitlab token");
    throw new Error(e);
  }
}

/**
 * check if token Expired
 *
 * @param {number} expires date token expires
 * @returns {boolean} true if expired, else false
 */
function tokenExpired(expires) {
  const expired = expires - new Date().getTime() / 1000;
  return expired > 0 ? false : true;
}

/**
 * Customize the config (init) for a fetch with our
 * own specific props
 * @remarks https://stackoverflow.com/questions/71551140/with-jsdoc-how-do-i-doc-a-param-type-to-match-a-param-used-in-a-global-method-l
 *
 * @param {string} accessToken access gitlabtoken
 * @param {RequestInit?} [config={}] fetch init object (https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters)

 * @returns {RequestInit} new Config
 */
function getCustomConfig(accessToken, config = {}) {
  let { accept, authorization } = getRequestHeaders(accessToken);
  const newConfig = {
    ...config,
    headers: {
      ...(config.headers ? config.headers : {}),
      Accept: accept,
      Authorization: authorization,
    },
  };
  return newConfig;
}

/**
 * request headers to forward along
 *
 * @param {string} accessToken access token
 * @returns {object} headers
 */
function getRequestHeaders(accessToken) {
  let headers = {
    accept: "application/json",
    "Content-Type": "application/json",
  };
  if (accessToken) {
    headers.authorization = `Bearer ${accessToken}`;
  }
  return headers;
}

fetchIntercept.register({
  request: (url, config) => {
    let tokensObj = sessionStorage.getItem(SESSION_KEYS.GITLAB_ACCESS_TOKEN)
      ? JSON.parse(sessionStorage.getItem(SESSION_KEYS.GITLAB_ACCESS_TOKEN))
      : null;
    if (!tokensObj) {
      console.log("no 'gitLabToken' access token found");
      return [url, config];
    } else {
      let { accessToken, expires, refreshToken } = tokensObj;
      if (url === "https://gitlab.com/oauth/token") {
        // we're already trying to get a new refreshtoken:
        return [url, getCustomConfig(accessToken, config)];
      }
      if (!tokenExpired(expires)) {
        return [url, getCustomConfig(accessToken, config)];
      } else {
        let refreshParams = {
          client_id: CLIENT_ID,
          code_verifier: sessionStorage.getItem(
            SESSION_KEYS.GITLAB_CODE_VERIFIER
          ),
          grant_type: "refresh_token",
          redirect_uri: window.location.origin + "/callback",
          refresh_token: refreshToken,
        };
        fetch("https://gitlab.com/oauth/token", {
          body: JSON.stringify(refreshParams),
          headers: {
            "Content-Type": "application/json",
          },
          method: "POST",
        }).then((response) => {
          if (response.status !== 200) {
            console.warn("An error occurred getting a refresh token");
          } else {
            // update session w. latest tokens:
            response.json().then((json) => {
              window.sessionStorage.setItem(
                SESSION_KEYS.GITLAB_ACCESS_TOKEN,
                JSON.stringify(json)
              );
              return [url, getCustomConfig(accessToken, config)];
            });
          }
        });
      }
    }
  },
});
