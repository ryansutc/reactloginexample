import { useContext } from 'react';

import {
  actions,
  SignInDispatchContext,
  SignInStateContext,
} from './AuthProvider';

function UserInfoPanel(props) {
  const state = useContext(SignInStateContext);
  const dispatch = useContext(SignInDispatchContext);
  const { userInfo } = state;

  return (
    <>
      <div>{`username: ${userInfo.username}`}</div>
      <div>{`name: ${userInfo.name}`}</div>
      <div></div>
      <a href={userInfo.web_url} title="go to gitlab page">
        <img src={userInfo.avatar_url} alt="avatar" style={{ width: "50px" }} />
      </a>
      <br />
      <button onClick={() => dispatch({ type: actions.SIGN_OUT })}>
        Sign out
      </button>
    </>
  );
}

export default UserInfoPanel;
