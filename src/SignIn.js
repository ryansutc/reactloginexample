import {
  useContext,
  useEffect,
} from 'react';

import {
  actions,
  SignInDispatchContext,
  SignInStateContext,
} from './AuthProvider';
import {
  fetchGitLabToken,
  getGitLabAuthorization,
  getGitLabProjectInfo,
  getGitLabUserInfo,
  getParamsFromGitLabCallback,
} from './helpers/SignInHelpers';
import { SESSION_KEYS } from './utils/constants';

function SignIn(props) {
  const dispatch = useContext(SignInDispatchContext);
  const state = useContext(SignInStateContext);
  const { isAuthenticated, isSigningIn, userInfo } = state;

  const signInViaSSO = () => {
    dispatch({ type: actions.SIGN_IN });
    getGitLabAuthorization();
  };

  useEffect(() => {
    // check if we got a callback,
    // if so send it to reducer to
    // capture in state:

    if (window.location.pathname === "/callback") {
      let params = document.location.search.replace("?", "");

      const { code, gitlabState } = getParamsFromGitLabCallback(params);

      window.history.replaceState({}, document.title, "/"); // reset browser url. remove args

      if (code && gitlabState) {
        fetchGitLabToken(
          code,
          sessionStorage.getItem(SESSION_KEYS.GITLAB_CODE_VERIFIER)
        ).then((json) => {
          if ("access_token" in json) {
            dispatch({
              type: actions.SIGN_IN_SUCCESS,
              payload: {
                accessToken: json.access_token,
                expires: json.created_at + json.expires_in,
                refreshToken: json.refresh_token,
              },
            });
          } else {
            dispatch({
              type: actions.SIGN_IN_ERROR,
              payload: {
                error: json.error,
              },
            });
          }
        });
      }
    }
  }, [dispatch]);

  useEffect(() => {
    if (isAuthenticated && !userInfo) {
      // get user info:
      getGitLabUserInfo().then((jsonInfo) => {
        if (jsonInfo.error) {
          dispatch({
            type: actions.SIGN_IN_ERROR,
            error: jsonInfo.error,
          });
        } else {
          dispatch({
            type: actions.USER_INFO_SUCCESS,
            payload: jsonInfo,
          });
        }
      });
      getGitLabProjectInfo().then((jsonInfo) => {
        if (jsonInfo.error) {
          dispatch({
            type: actions.SIGN_IN_ERROR,
            error: jsonInfo.error,
          });
        } else {
          dispatch({
            type: actions.PROJECT_INFO_SUCCESS,
            payload: jsonInfo,
          });
        }
      });
    }
  }, [isAuthenticated, dispatch, userInfo]);

  return (
    <div>
      {isAuthenticated ? (
        <div>Authenticated..getting user info</div>
      ) : (
        <div>You are not authenticated</div>
      )}

      <button disabled={isSigningIn} onClick={signInViaSSO}>
        {`Sign in`}
      </button>
    </div>
  );
}

export default SignIn;
